### These packages will be added in the default selection when I will find time to document them.
# It should be safe but you still need to be ***VERY** careful).

# REMINDER : "Safe" only means "does not cause bootloop". 
# These packages can definitely break features (but you can easily reinstall them with my script)

declare -a pending=(

	############# AOSP #############
	"com.android.keyguard"
	"com.android.hwmirror" # Huawei Mirror app.
	"com.android.dreams" # AOSP screensaver



	####### THIRD PARTY ########
	"com.microsoft.translator"
	"com.realvnc.android.remote"
	"com.hicloud.android.clone"
	"com.hisi.mapcon"              # "wi-fi calling" feature
	"com.ironsource.appcloud.oobe.huawei"
	"com.qti.confuridialer"
	# "com.qualcomm.cabl"
	# "com.qualcomm.svi"
	# "com.qualcomm.uimremoteserver"
	# "com.qualcomm.wfd.service"


	############ SAMSUNG ############
	"com.samsung.android.unifiedprofile"
	"com.samsung.android.widgetapp.yagooedge.sport"
	"com.samsung.faceservice"
	"com.samsung.fresco.logging"
	"com.samsung.knox.securefolder.setuppage"
	"com.sec.android.widgetapp.webmanual"
	"com.sec.enterprise.knox.cloudmdm.smdms"
	"com.samsung.android.app.accesscontrol"
	"com.samsung.android.app.color"
	"com.samsung.android.app.multiwindow"
	"com.samsung.android.coreapps"
	"com.samsung.android.rubin.app"
	"com.samsung.android.sdk.handwriting"
	"com.samsung.android.video"
	"com.samsung.app.jansky"
	"com.samsung.app.newtrim"
	"com.samsung.tmowfc.wfccontroller"
	"com.samsung.tmowfc.wfcpref"
	"com.sec.android.app.bluetoothtest"
	"com.sec.android.app.chromecustomizations"
	"com.sec.android.app.DataCreate"
	"com.sec.android.app.factorykeystring"
	"com.sec.android.app.hwmoduletest"
	"com.sec.android.app.parser"
	"com.sec.android.app.ringtoneBR"
	"com.sec.android.app.servicemodeapp"
	"com.sec.android.app.soundalive"
	"com.sec.android.app.sysscope"
	"com.sec.android.app.vepreload"
	"com.sec.android.app.wlantest"
	"com.sec.android.diagmonagent"
	#"com.sec.android.provider.badge"
	#"com.sec.android.providers.security"
	"com.sec.android.soagent"
	"com.sec.android.splitsound"
	"com.sec.app.RilErrorNotifier"
	"com.sec.factory.camera"
	"com.sec.hearingadjust"
	"com.sec.hiddenmenu"
	#"com.sec.phone"
	"com.sec.vsim.ericssonnsds.webapp"


	############## SONY #############	
	"com.sonymobile.devicesecurity.service"
	"com.sonymobile.home.product.res.overlay"
	"com.sonymobile.indeviceintelligence"
	"com.sonymobile.swiqisystemservice"
	"com.sonymobile.themes.xperialoops2"
	"com.sonymobile.xperiaxlivewallpaper"
	"com.sonymobile.xperiaxlivewallpaper.product.res.overlay"


	############ HUAWAI #############
	"com.huawei.android.pushagent"
	"com.huawei.android.thememanager"
	"com.huawei.android.totemweatherapp"
	"com.huawei.android.totemweatherwidget"
	"com.huawei.android.wfdft"
	"com.huawei.bd"
	"com.huawei.contacts.sync"
	"com.huawei.HwMultiScreenShot" # screenshot with scroll
	"com.huawei.KoBackup"
	"com.huawei.livewallpaper.artflower"
	"com.huawei.livewallpaper.flowersbloom"
	"com.huawei.livewallpaper.mountaincloud"
	"com.huawei.livewallpaper.naturalgarden"
	"com.huawei.livewallpaper.ripplestone"
	"com.huawei.mirrorlink"
	"com.huawei.phoneservice" # HiCare app.
	"com.huawei.search" # search apps on launcher (if yoıu disbale this you lost search feature of apps. not important for many users.)
	"com.huawei.securitymgr"
	"com.huawei.tips"
	"com.huawei.vassistant"        # HiVoice app

	# com.huawei.systemmanager # It can make any app not working in background.
	)
